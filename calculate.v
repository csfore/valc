module main

import math

fn calculate(one string, two string, operator string) !string {
	return match operator {
		'+' {
			(one.f64() + two.f64()).str()
		}
		'-' {
			(one.f64() - two.f64()).str()
		}
		'*' {
			(one.f64() * two.f64()).str()
		}
		'/' {
			(one.f64() / two.f64()).str()
		}
		'^' {
			(math.pow(one.f64(), two.f64())).str()
		}
		'%' {
			(math.fmod(one.f64(), two.f64())).str()
		}
		else {
			return error('An unknown operator was found.')
		}
	}
}
