module main

import os

const (
	operators = ['^', '*', '/', '%', '+', '-']
)

fn main() {

	for {
		expr := os.input('')

		mut expr_tmp := []string{}
		mut expr_split := []string{}

		for index, item in expr {
			// If the item's an operator, add the tmp array and clear it
			if item.ascii_str() in operators {
				joined := expr_tmp.join('')
				expr_split << joined
				expr_split << item.ascii_str()
				expr_tmp.clear()
			} else {
				expr_tmp << item.ascii_str()
			}

			if index == expr.len - 1 {
				expr_split << expr_tmp.join('')
			}
		}

		mut mode := 0

		for mode <= 3 {
			for index, item in expr_split {
				if item == operators[mode] {
					// Insert the result at the end of the array
					expr_split.insert(index + 2, calculate(expr_split[index - 1], expr_split[index + 1],
						item) or {
						eprintln(err)
						return
					})
					expr_split.delete_many(index - 1, index)
				}
			}
			mode++
		}
		println(expr_split[expr_split.len - 1])
	}
}